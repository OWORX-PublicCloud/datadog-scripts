﻿#########################################################
#This script will Invoke the run command to install the Datadog agent
#On all running Windows, RedHat, Ubuntu, CentOS VM's with "oworxmon:true" tag
#########################################################

#Set Datadog API key of customer
$datadogApiKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

#List which Azure Subscriptions to install Datadog across
$subIdList = @(
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
)

#Do not change these variables
$datadogSite = "datadoghq.eu"
$datadogMajorVersion = 7

#Download installer scripts to your local temp folder
#Windows
New-Item -ItemType Directory "C:\Windows\temp\ddinstall" -Force
$url = "https://bitbucket.org/OWORX-PublicCloud/datadog-agent/raw/master/azure/dd-azure-windows-install.ps1"
$output = "C:\Windows\temp\ddinstall\dd-azure-windows-install.ps1"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)

#Linux
New-Item -ItemType Directory "C:\Windows\temp\ddinstall" -Force
$url = "https://bitbucket.org/OWORX-PublicCloud/datadog-agent/raw/master/azure/dd-azure-linux-install.sh"
$output = "C:\Windows\temp\ddinstall\dd-azure-linux-install.sh"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)

#Across all subscriptions in list invoke run command on all running Windows, RedHat, Ubuntu, CentOS VM's with "oworxmon:true" tag
foreach ($subId in $subIdList){

    Select-AzSubscription -SubscriptionId $subId
    $vmList = Get-AzVM

    foreach ($vm in $vmList)
        {
        $vmObj = Get-AzVM -ResourceGroupName $vm.ResourceGroupName -Name $vm.Name
        $vmStatus = Get-AzVM -ResourceGroupName $vm.ResourceGroupName -Name $vm.Name -Status
        $vmOS = $vmObj.StorageProfile.OsDisk.OsType
        $vmImageRefOffer = $vmObj.StorageProfile.ImageReference.Offer
    
        #Windows VM
        if (($vmObj.Tags.oworxmon -eq "true")`
            -and ($vmStatus.Statuses[1].DisplayStatus -eq "VM Running")`
            -and ($vmImageRefOffer -eq "WindowsServer")){
            Write-Host -ForegroundColor Green $vmObj.Name "Oworxmon tag true - Windows Server - Installing Datadog"
            Invoke-AzVMRunCommand `
                -ResourceGroupName $vm.ResourceGroupName `
                -Name $vm.Name `
                -CommandId 'RunPowerShellScript' `
                -ScriptPath "C:\Windows\temp\ddinstall\dd-azure-windows-install.ps1" `
                -Parameter @{
                    "ApiKey" = "$datadogApiKey";
                    "Site" = "$datadogSite";
                    "MajorVersion" = "$datadogMajorVersion"}`
                -AsJob
        }

        #CentOS, RHEL, Ubuntu VM
        elseif (($vmObj.Tags.oworxmon -eq "true")`
            -and ($vmImageRefOffer -in ("CentOS","RHEL","UbuntuServer"))){
            Write-Host -ForegroundColor Green $vmObj.Name "Oworxmon tag true - $vmImageRefOffer Server - Installing Datadog"
            Invoke-AzVMRunCommand  `
            -ResourceGroupName $vm.ResourceGroupName `
            -Name $vm.Name `
            -CommandId "RunShellScript" `
            -ScriptPath "C:\Windows\temp\ddinstall\dd-azure-linux-install.sh" `
            -Parameter @{
                "APIKey" = "$datadogApiKey";
                "Site" = "$datadogSite";
                "MajorVersion" = "$datadogMajorVersion"}`
            -AsJob
            }
        elseif ($vmObj.Tags.oworxmon -eq "false"){
            Write-Host -ForegroundColor Red $vmObj.Name "Oworxmon tag false"
        }
        elseif ($vmStatus.Statuses[1].DisplayStatus -ne "VM Running"){
            Write-Host -ForegroundColor Red $vmObj.Name "VM not running"
        }
        else {
            Write-Host -ForegroundColor Red $vmObj.Name "Oworxmon tag not present"
        } 
    }
}

Remove-Item "C:\Windows\temp\ddinstall" -Recurse