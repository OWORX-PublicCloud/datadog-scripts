﻿###This script will apply tag oworxmon:true to specific VMs within the specified subscription###

#List Subscription ID (Only subscriptions under Oworx Managed Support)
$subId = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'

#List VM names
$vmlist = @(
'VMNAME'
'VMNAME'
)

$properties = [ordered]@{
        SubscriptionName = [string]
        SubscriptionID = [string]
        ResourceGroup = [string]
        VMName = [string]
        oworxmonTagOldValue = [string]
        oworxmonTagNewValue = [string]
        }

$sub = Get-AzSubscription -SubscriptionId $subId
$sub | Select-AzSubscription

$tObj = @()
ForEach ($vmName in $vmlist){

    $vm = Get-AzResource -ResourceType Microsoft.Compute/virtualMachines -Name $vmName

    if ($vm.Count -eq 2) {
        Write-Host "$vmname DUPLICATE"
    
        #First VM duplicate
        $vmObj = Get-AzVM -ResourceGroupName $vm[0].ResourceGroupName -Name $vm[0].Name

        $obj = New-Object -TypeName psobject -Property $properties
        $obj.SubscriptionName = $sub.Name
        $obj.SubscriptionID = $sub.Id
        $obj.ResourceGroup = $vmObj.ResourceGroupName
        $obj.VMName = $vmObj.Name
        $obj.oworxmonTagOldValue = $vmObj.Tags.oworxmon
        $oworxmonTag = @{oworxmon="true"}
        Update-AzTag -ResourceId $vmObj.Id -Tag $oworxmonTag -Operation Merge
        $obj.oworxmonTagNewValue = (Get-AzVm -ResourceGroupName $vmObj.ResourceGroupName -Name $vmObj.Name).Tags.oworxmon
        $tobj += $obj

        #Second VM duplicate
        $vmObj = Get-AzVM -ResourceGroupName $vm[1].ResourceGroupName -Name $vm[1].Name

        $obj = New-Object -TypeName psobject -Property $properties
        $obj.SubscriptionName = $sub.Name
        $obj.SubscriptionID = $sub.Id
        $obj.ResourceGroup = $vmObj.ResourceGroupName
        $obj.VMName = $vmObj.Name
        $obj.oworxmonTagOldValue = $vmObj.Tags.oworxmon
        $oworxmonTag = @{oworxmon="true"}
        Update-AzTag -ResourceId $vmObj.Id -Tag $oworxmonTag -Operation Merge
        $obj.oworxmonTagNewValue = (Get-AzVm -ResourceGroupName $vmObj.ResourceGroupName -Name $vmObj.Name).Tags.oworxmon
        $tobj += $obj
        }

    else {
        $vmObj = Get-AzVM -ResourceGroupName $vm.ResourceGroupName -Name $vm.Name

        $obj = New-Object -TypeName psobject -Property $properties
        $obj.SubscriptionName = $sub.Name
        $obj.SubscriptionID = $sub.Id
        $obj.ResourceGroup = $vmObj.ResourceGroupName
        $obj.VMName = $vmObj.Name
        $obj.oworxmonTagOldValue = $vmObj.Tags.oworxmon
        $oworxmonTag = @{oworxmon="true"}
        Update-AzTag -ResourceId $vmObj.Id -Tag $oworxmonTag -Operation Merge
        $obj.oworxmonTagNewValue = (Get-AzVm -ResourceGroupName $vmObj.ResourceGroupName -Name $vmObj.Name).Tags.oworxmon
        $tobj += $obj
        }
    }

$tobj | Format-Table
$tobj | Export-Csv -Path "$env:USERPROFILE\Desktop\Oworxmon tag rollout.csv"