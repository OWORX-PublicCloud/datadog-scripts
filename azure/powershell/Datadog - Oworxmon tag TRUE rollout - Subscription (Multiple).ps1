﻿###This script will apply tag oworxmon:true to every VM within the specified subscriptions###

#List Subscription ID's (Only subscriptions under Oworx Managed Support)
$subIdList = @(
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
)

$properties = [ordered]@{
        SubscriptionName = [string]
        SubscriptionID = [string]
        ResourceGroup = [string]
        VMName = [string]
        oworxmonTagOldValue = [string]
        oworxmonTagNewValue = [string]
        }

$tObj = @()

foreach ($subId in $subIdList){
    $sub = Select-AzSubscription -SubscriptionId $subId
    
    $vmList = Get-AzVM

    ForEach ($vmObj in $vmList){
        $obj = New-Object -TypeName psobject -Property $properties
        $obj.SubscriptionName = $sub.Name
        $obj.SubscriptionID = $sub.Id
        $obj.ResourceGroup = $vmObj.ResourceGroupName
        $obj.VMName = $vmObj.Name
        $obj.oworxmonTagOldValue = $vmObj.Tags.oworxmon
        $oworxmonTag = @{oworxmon="true"}
        Update-AzTag -ResourceId $vmObj.Id -Tag $oworxmonTag -Operation Merge
        $obj.oworxmonTagNewValue = (Get-AzVm -ResourceGroupName $vmObj.ResourceGroupName -Name $vmObj.Name).Tags.oworxmon
        $tobj += $obj
        }
    }

$tobj | Format-Table
$tobj | Export-Csv -Path "$env:USERPROFILE\Desktop\Oworxmon tag rollout.csv"