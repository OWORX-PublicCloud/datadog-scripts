﻿###This script deploys the log forwarding solution which consists of an Azure event hub and Azure function.
###The second part of the script configures the Azure Activity Log diagnostics settings of each subscription listed.

#Set the following variables before running the script!!!

#The customer name must be all lowercase letters and under 15 characters e.g. "testcustomer".
$customerName =  "CUSTOMERNAME"

#This will be the subscription where the log forwarding solution is deployed. You want to choose a subscription which will not be removed in the future e.g. a shared services, or production subscription.
$monitoringSubId = "MONITORINGSUBID"

#Use the primary location of where the majority of the customers solution is located e.g. "West Europe"
$location = "LOCATION"

#This is the API key of the customer, review the onboarding guide for its location.
$datadogApiKey = "DATADOGAPIKEY"

#Here you need to list all subscription ID's under our Managed Support.
$subIdList = @(
$monitoringSubId
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
)

#Do not change these variables
$monitoringRsg = "OWORX-MONITORING"
$templateFile = "https://bitbucket.org/OWORX-PublicCloud/datadog-agent/raw/master/azure/DatadogDeployment-Template.json"
$templateFile2 = "https://bitbucket.org/OWORX-PublicCloud/datadog-agent/raw/master/azure/DatadogActivityLogDiagnosticsConfig-Template.json"
$functionCode = (New-Object System.Net.WebClient).DownloadString("https://raw.githubusercontent.com/DataDog/datadog-serverless-functions/master/azure/activity_logs_monitoring/index.js")

$paramObj = @{
        #General
        "tags" = @{
            "oworxmon" = "true"
            }
        "subscriptionId" = "$monitoringSubId"
        "resourceGroupName" = "$monitoringRsg"
        "location" = "$location"
        
        #EventHub
        "eventHubNamespaceName" = "oworxmonnamespace$customerName"
        "eventHubName" = "oworxmoneventhub"

        #Storage Account
        "storageAccountName" = "oworxmon$customerName"
        
        #App Service Plan
        "appServicePlanName" = "OWORX-MONITORING-ASP"
        
        #Function App
        "functionAppName" = "oworxmonfun$customerName"
        
        #Function
        "functionName" = "oworxmonfun$customerName"
        "functionCode" = $functionCode

        #Datadog
        "datadogApiKey" = $datadogApiKey
        "datadogSite" = "datadoghq.eu"
        "datadogTags" = "oworxmon:true"
}

#Deploy RSG/StorageAccount/EventHub/Function        
Select-AzSubscription -SubscriptionId $monitoringSubId
New-AzResourceGroup -Name $monitoringRsg -Location $location
New-AzResourceGroupDeployment `
    -ResourceGroupName $monitoringRsg `
    -TemplateUri $templateFile `
    -TemplateParameterObject $paramObj

#Configure Azure Activity Log Diagnostics settings to forward to event hub
foreach ($subId in $subIdList){

    $paramObj = @{
                #General
                "subscriptionId" = "$monitoringSubId"
                "resourceGroupName" = "$monitoringRsg"
        
                #EventHub
                "eventHubNamespaceName" = "oworxmonnamespace$customerName"
                "eventHubName" = "oworxmoneventhub"
                }

    Select-AzSubscription -SubscriptionId $subId

        If ((Get-AzResourceGroup -Name $monitoringRsg -ErrorAction SilentlyContinue).ResourceGroupName -eq $monitoringRsg) {
            New-AzSubscriptionDeployment `
                -Location $location `
                -ResourceGroupName $monitoringRsg `
                -TemplateUri $templateFile2 `
                -TemplateParameterObject $paramObj
                }
        ElseIf ((Get-AzResourceGroup -Name $monitoringRsg -ErrorAction SilentlyContinue).ResourceGroupName -ne $monitoringRsg) {
            New-AzResourceGroup -Name $monitoringRsg -Location $location
            New-AzSubscriptionDeployment `
                -Location $location `
                -ResourceGroupName $monitoringRsg `
                -TemplateUri $templateFile2 `
                -TemplateParameterObject $paramObj
                }
}