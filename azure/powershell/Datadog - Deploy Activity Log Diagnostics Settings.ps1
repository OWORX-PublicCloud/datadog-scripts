﻿###This script configures the Azure Activity Log diagnostics settings of each subscription listed to forward to the event hub.

#Set the following variables before running the script!!!

#The customer name must be all lowercase letters and under 15 characters e.g. "testcustomer".
$customerName =  "CUSTOMERNAME"

#This is the subscription where the log forwarding solution is deployed.
$monitoringSubId = "MONITORINGSUBID"

#Use the primary location of where the majority of the customers solution is located e.g. "West Europe"
$location = "LOCATION"

#Here you need to list all subscription ID's under our Managed Support.
$subIdList = @(
$monitoringSubId
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
)

#Do not change these variables
$monitoringRsg = "OWORX-MONITORING"
$templateFile = "https://bitbucket.org/OWORX-PublicCloud/datadog-agent/raw/master/azure/DatadogActivityLogDiagnosticsConfig-Template.json"

#Configure Azure Activity Log Diagnostics settings to forward to event hub
foreach ($subId in $subIdList){

    $paramObj = @{
                #General
                "subscriptionId" = "$monitoringSubId"
                "resourceGroupName" = "$monitoringRsg"
        
                #EventHub
                "eventHubNamespaceName" = "oworxmonnamespace$customerName"
                "eventHubName" = "oworxmoneventhub"
                }

    Select-AzSubscription -SubscriptionId $subId

        If ((Get-AzResourceGroup -Name $monitoringRsg -ErrorAction SilentlyContinue).ResourceGroupName -eq $monitoringRsg) {
            New-AzSubscriptionDeployment `
                -Location $location `
                -ResourceGroupName $monitoringRsg `
                -TemplateUri $templateFile `
                -TemplateParameterObject $paramObj
                }
        ElseIf ((Get-AzResourceGroup -Name $monitoringRsg -ErrorAction SilentlyContinue).ResourceGroupName -ne $monitoringRsg) {
            New-AzResourceGroup -Name $monitoringRsg -Location $location
            New-AzSubscriptionDeployment `
                -Location $location `
                -ResourceGroupName $monitoringRsg `
                -TemplateUri $templateFile `
                -TemplateParameterObject $paramObj
                }
}