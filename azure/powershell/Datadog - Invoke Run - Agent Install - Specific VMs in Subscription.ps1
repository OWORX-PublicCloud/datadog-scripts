﻿#########################################################
#Invoke run command to install datadog agent
#On All running Windows VM's with "oworxmon:true" tag
#########################################################

#Set Datadog API key of customer
$datadogApiKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

#Set Azure Subscriptions to install Datadog across
$subId = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'

#List VM(s) to install Datadog agent on
$vmlist = @(
'VMNAME'
'VMNAME'
)

#Do not change these variables
$datadogSite = "datadoghq.eu"
$datadogMajorVersion = 7

#Download installer scripts to your local temp folder
#Windows
New-Item -ItemType Directory "C:\Windows\temp\ddinstall" -Force
$url = "https://bitbucket.org/OWORX-PublicCloud/datadog-agent/raw/master/azure/dd-azure-windows-install.ps1"
$output = "C:\Windows\temp\ddinstall\dd-azure-windows-install.ps1"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)

#Linux
New-Item -ItemType Directory "C:\Windows\temp\ddinstall" -Force
$url = "https://bitbucket.org/OWORX-PublicCloud/datadog-agent/raw/master/azure/dd-azure-linux-install.sh"
$output = "C:\Windows\temp\ddinstall\dd-azure-linux-install.sh"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)


#For subscription specified invoke run command on listed VMs that are in running state with Os's Windows, RedHat, Ubuntu, CentOS and have "oworxmon:true" tag
Select-AzSubscription -SubscriptionId $subId

foreach ($vmName in $vmlist)
    {
    $vm = Get-AzResource -ResourceType Microsoft.Compute/virtualMachines -Name $vmName
    $vmObj = Get-AzVM -ResourceGroupName $vm.ResourceGroupName -Name $vm.Name
    $vmStatus = Get-AzVM -ResourceGroupName $vm.ResourceGroupName -Name $vm.Name -Status
    $vmOS = $vmObj.StorageProfile.OsDisk.OsType
    $vmImageRefOffer = $vmObj.StorageProfile.ImageReference.Offer

    #Windows VM
    if (($vmobj.Tags.oworxmon -eq "true")`
        -and ($vmstatus.Statuses[1].DisplayStatus -eq "VM Running")`
        -and ($vmOS -eq "Windows")){
        Write-Host -ForegroundColor Green $vmobj.Name "Oworxmon tag true - Windows Server - Installing Datadog"
        Invoke-AzVMRunCommand `
            -ResourceGroupName $vm.ResourceGroupName `
            -Name $vm.Name `
            -CommandId 'RunPowerShellScript' `
            -ScriptPath "C:\Windows\temp\ddinstall\dd-azure-windows-install.ps1" `
            -Parameter @{
                "ApiKey" = "$datadogApiKey";
                "Site" = "$datadogSite";
                "MajorVersion" = "$datadogMajorVersion"}`
            -AsJob
    }

    #CentOS, RHEL, Ubuntu VM
    elseif (($vmobj.Tags.oworxmon -eq "true")`
        -and ($vmImageRefOffer -in ("CentOS","RHEL","UbuntuServer"))){
        Write-Host -ForegroundColor Green $vmobj.Name "Oworxmon tag true - $vmImageRefOffer Server - Installing Datadog"
        Invoke-AzVMRunCommand  `
        -ResourceGroupName $vm.ResourceGroupName `
        -Name $vm.Name `
        -CommandId "RunShellScript" `
        -ScriptPath "C:\Windows\temp\ddinstall\dd-azure-linux-install.sh" `
        -Parameter @{
            "APIKey" = "$datadogApiKey";
            "Site" = "$datadogSite";
            "MajorVersion" = "$datadogMajorVersion"}`
        -AsJob
        }
    elseif ($vmobj.Tags.oworxmon -eq "false"){
        Write-Host -ForegroundColor Red $vmobj.Name "Oworxmon tag false"
    }
    elseif ($vmstatus.Statuses[1].DisplayStatus -ne "VM Running"){
        Write-Host -ForegroundColor Red $vmobj.Name "VM not running"
    }
    else {
        Write-Host -ForegroundColor Red $vmobj.Name "Oworxmon tag not present"
    } 
 }

Remove-Item "C:\Windows\temp\ddinstall" -Recurse